import 'element-ui/lib/theme-chalk/index.css';
import Vue from 'vue';
import localforage from 'localforage';
import Element from 'element-ui';
import App from './App';
import router from './router';
import store from './store/main';


Vue.prototype.$localforage = localforage;

Vue.use(Element);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
