/* eslint-disable indent */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loading: false,
        hideSearchTool: false,
        noMatch: false,
        selected: {
            one: null,
            two: null,
            three: null,
            four: null,
            five: null,
          },
        searchResults: [],
    },
    mutations: {
        SET_NOMATCH(state, payload) {
            state.noMatch = payload;
        },
        SET_SELECTED(state, payload) {
            state.selected[payload] = payload;
        },
        SET_LOADING(state, payload) {
            state.loading = payload;
        },
        SET_SEARCHTOOL(state, payload) {
            state.hideSearchTool = payload;
        },
        SET_SEARCHRESULTS(state, payload) {
            state.searchResults = payload;
        },
    },
    actions: {
        changeNoMatch({ commit }, payload) {
            commit('SET_NOMATCH', payload);
        },
        changeSelected({ commit }, payload) {
          commit('SET_SELECTED', payload);
        },
        changeLoading({ commit }, payload) {
            commit('SET_LOADING', payload);
        },
        changeHideSearchTool({ commit }, payload) {
            commit('SET_SEARCHTOOL', payload);
        },
        updateSearchResults({ commit }, payload) {
            commit('SET_SEARCHRESULTS', payload);
        },
    },
    getters: {
        showResult(state) {
            if (state.searchResults.length > 0 && state.noMatch === false) {
              return true;
            }
              return false;
          },
    },

});

