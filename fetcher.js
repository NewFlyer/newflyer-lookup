var request = require("request");
var fs = require("fs")



let assetArray = []
let count = 0
let obj = {}


function fetchAssets(offset) {
    request({ method: 'POST',
    url: 'https://newflyer.custhelp.com/services/rest/connect/v1.3/analyticsReportResults',
    headers: 
     { 'cache-control': 'no-cache',
       authorization: 'Basic b3N2Y19zb2FwOjUlRDEybk8zREc1UQ==',
       'content-type': 'application/json' },
    body: { id: 102355, offset: offset },
    json: true }, function (error, response, body) {
        if (error) throw new Error(error);
          count = body.count
          rows = body.rows
          console.log('Response returned: ' + count + ' records')
          if (count === 10000) {
            rows.forEach(row => {
                if (!obj[row[0]]) {
                    obj[row[0]] = row[1]
                }
                if (row[2] !== null) {
                    let srNumber = row[2].replace("SR-", "")
                    assetArray.push([row[0],srNumber,row[3],row[4]])
                } else {
                    assetArray.push([row[0],row[2],row[3],row[4]])
                }
            })
            console.log('Requesting more...')
            offset = offset + 10000
            console.log('New offset: ' + offset)
            fetchAssets(offset)
          } else {
            rows.forEach(row => {
                if (!obj[row[0]]) {
                    obj[row[0]] = row[1]
                }
                if (row[2] !== null) {
                    let srNumber = row[2].replace("SR-", "")
                    assetArray.push([row[0],srNumber,row[3],row[4]])
                } else {
                    assetArray.push([row[0],row[2],row[3],row[4]])
                }
            })
            
            let stringifyArray = JSON.stringify(assetArray)
            let stringifyObj = JSON.stringify(obj)
            fs.writeFile('./src/assets/orgData.json', stringifyObj, function (err) {
                if (err) throw new Error(err);
            })
            fs.writeFile('./src/assets/assetDataAll.json', stringifyArray, function (err) {
                if (err) throw new Error(err);
            })
            return console.log('Finished')
          }
      });
}

fetchAssets(count)
